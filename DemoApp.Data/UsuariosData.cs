﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using DemoApp.BE;
using Newtonsoft.Json;

namespace DemoApp.Data
{
	// Token: 0x02000003 RID: 3
	public class UsuariosData
	{
		// Token: 0x06000002 RID: 2 RVA: 0x000020CC File Offset: 0x000002CC
		public UsuariosData(string serverfolder)
		{
			this.Folder = serverfolder;
		}

		// Token: 0x06000003 RID: 3 RVA: 0x000020E8 File Offset: 0x000002E8
		public Usuario Autenticar(string usuario, string password)
		{
			string json = File.ReadAllText(this.Folder + "\\dbdata\\userscredentials.json");
			json = Crypto.Decrypt(ConfigurationManager.AppSettings["IV"], json, true);
			string hash = this.GetMd5Hash(password);
			IEnumerable<Usuario> result = from u in JsonConvert.DeserializeObject<List<Usuario>>(json)
			where u.UserName == usuario && u.Password == hash
			select u;
			if (result.Any<Usuario>())
			{
				return result.FirstOrDefault<Usuario>();
			}
			return null;
		}

		// Token: 0x06000004 RID: 4 RVA: 0x00002164 File Offset: 0x00000364
		private string GetMd5Hash(string input)
		{
			StringBuilder sBuilder = new StringBuilder();
			using (MD5 md5Hash = MD5.Create())
			{
				byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
				for (int i = 0; i < data.Length; i++)
				{
					sBuilder.Append(data[i].ToString("x2"));
				}
			}
			return sBuilder.ToString();
		}

		// Token: 0x04000001 RID: 1
		private string Folder = string.Empty;
	}
}
