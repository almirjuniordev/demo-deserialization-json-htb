﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace DemoApp.Data
{
	// Token: 0x02000002 RID: 2
	public static class Crypto
	{
		// Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
		public static string Decrypt(string key, string cipherString, bool useHashing)
		{
			byte[] toEncryptArray = Convert.FromBase64String(cipherString);
			byte[] keyArray;
			if (useHashing)
			{
				MD5CryptoServiceProvider md5CryptoServiceProvider = new MD5CryptoServiceProvider();
				keyArray = md5CryptoServiceProvider.ComputeHash(Encoding.UTF8.GetBytes(key));
				md5CryptoServiceProvider.Clear();
			}
			else
			{
				keyArray = Encoding.UTF8.GetBytes(key);
			}
			TripleDESCryptoServiceProvider tripleDESCryptoServiceProvider = new TripleDESCryptoServiceProvider();
			tripleDESCryptoServiceProvider.Key = keyArray;
			tripleDESCryptoServiceProvider.Mode = CipherMode.ECB;
			tripleDESCryptoServiceProvider.Padding = PaddingMode.PKCS7;
			byte[] resultArray = tripleDESCryptoServiceProvider.CreateDecryptor().TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
			tripleDESCryptoServiceProvider.Clear();
			return Encoding.UTF8.GetString(resultArray);
		}
	}
}
