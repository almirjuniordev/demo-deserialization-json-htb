﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using DemoApp.BE;
using DemoApp.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace DemoAppExplanaiton.Controllers
{
    public class AccountController : ApiController
    {
        // Token: 0x06000012 RID: 18 RVA: 0x000023F8 File Offset: 0x000005F8
        [HttpPost]
        [Route("api/token")]
        public HttpResponseMessage Login(Usuario login)
        {
            string mappedPath = HttpContext.Current.Server.MapPath("~");
            UsuariosData mngUsuarios = new UsuariosData(mappedPath);
            Usuario usr = mngUsuarios.Autenticar(login.UserName, login.Password);
            if (usr == null)
            {
                return base.Request.CreateResponse(HttpStatusCode.NotFound, "User Not Exists");
            }
            if (usr.Rol != "Administrator")
            {
                return base.Request.CreateResponse(HttpStatusCode.Forbidden, "You don't have permissions to access this site.");
            }
            HttpCookie cookie = new HttpCookie("OAuth2");
            cookie.Expires = DateTime.Now.AddMinutes(2.0);
            byte[] bytes = Encoding.Default.GetBytes(JsonConvert.SerializeObject(usr));
            cookie.Value = Convert.ToBase64String(bytes);
            HttpContext.Current.Response.Cookies.Add(cookie);
            return base.Request.CreateResponse(HttpStatusCode.Accepted);
        }

        // Token: 0x06000013 RID: 19 RVA: 0x000024E4 File Offset: 0x000006E4
        [Route("api/Account/")]
        [HttpGet]
        public HttpResponseMessage GetInfo()
        {
            string cookie = HttpContext.Current.Request.Headers["Bearer"];
            HttpResponseMessage result;
            try
            {
                byte[] b = Convert.FromBase64String(cookie);
                string b2 = Encoding.UTF8.GetString(b);

                object obj = JsonConvert.DeserializeObject<object>(b2, new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.All
                });

                JObject users = (JObject)obj;
                result = base.Request.CreateResponse(HttpStatusCode.OK, users);
            }
            catch (FormatException fe)
            {
                result = base.Request.CreateErrorResponse(HttpStatusCode.InternalServerError, new Exception("Invalid format base64"));
            }
            catch (JsonReaderException je)
            {
                result = base.Request.CreateErrorResponse(HttpStatusCode.InternalServerError, new Exception("Cannot deserialize Json.Net Object"));
            }
            return result;
        }
    }
}