﻿using System;

namespace DemoApp.BE
{
	// Token: 0x02000005 RID: 5
	public class Usuario
	{
		// Token: 0x1700000C RID: 12
		// (get) Token: 0x0600001A RID: 26 RVA: 0x00002123 File Offset: 0x00000323
		// (set) Token: 0x0600001B RID: 27 RVA: 0x0000212B File Offset: 0x0000032B
		public int Id { get; set; }

		// Token: 0x1700000D RID: 13
		// (get) Token: 0x0600001C RID: 28 RVA: 0x00002134 File Offset: 0x00000334
		// (set) Token: 0x0600001D RID: 29 RVA: 0x0000213C File Offset: 0x0000033C
		public string UserName { get; set; }

		// Token: 0x1700000E RID: 14
		// (get) Token: 0x0600001E RID: 30 RVA: 0x00002145 File Offset: 0x00000345
		// (set) Token: 0x0600001F RID: 31 RVA: 0x0000214D File Offset: 0x0000034D
		public string Password { get; set; }

		// Token: 0x1700000F RID: 15
		// (get) Token: 0x06000020 RID: 32 RVA: 0x00002156 File Offset: 0x00000356
		// (set) Token: 0x06000021 RID: 33 RVA: 0x0000215E File Offset: 0x0000035E
		public string Name { get; set; }

		// Token: 0x17000010 RID: 16
		// (get) Token: 0x06000022 RID: 34 RVA: 0x00002167 File Offset: 0x00000367
		// (set) Token: 0x06000023 RID: 35 RVA: 0x0000216F File Offset: 0x0000036F
		public string Rol { get; set; }
	}
}
