﻿using System;

namespace DemoApp.BE
{
	// Token: 0x02000004 RID: 4
	public class PostParmas
	{
		// Token: 0x1700000B RID: 11
		// (get) Token: 0x06000017 RID: 23 RVA: 0x0000210A File Offset: 0x0000030A
		// (set) Token: 0x06000018 RID: 24 RVA: 0x00002112 File Offset: 0x00000312
		public string Value { get; set; }
	}
}
